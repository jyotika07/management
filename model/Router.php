<?php
namespace model;
use controller;
/*
using routes url 
 */
class Router {
    public $routes = [
        'GET'=>[],
        'POST'=>[]
    ];
    public function get($uri,$controller){
        $this->routes['GET'][$uri]=$controller;
    }
    public function post($uri,$controller){
        $this->routes['POST'][$uri]=$controller;
    }
    public function direct($uri,$requestType) {
        if(array_key_exists($uri, $this->routes[$requestType])) {
            return $this->callAction(
                ...explode('@', $this->routes[$requestType][$uri])
            );
        }
        throw new Exception("Error Processing Request");
    }
    public function callAction($controller, $action)
    {
        $controller = "controller\\{$controller}";
        $controller = new $controller;
        if(! method_exists($controller, $action)) 
        {
            throw new Exception("Error Processing Request");
        }
        return $controller->$action();
    }
}
