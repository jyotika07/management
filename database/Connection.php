<?php
/*
*create connection from database
 */
namespace management\database;
use PDO;
use PDOException;
class Connection
{
    protected $pdo;
    private $database = "blog";
    private $username = "root";
    private $password = "root";
    private $options = [
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
    ];
    public function __construct() {
        
        try {
            $this->pdo = new PDO(
                            'mysql:host=localhost;dbname='.$this->database,
                            $this->username,
                            $this->password,
                            $this->options
                        );
           echo 'connect database';
        }
        catch (PDOException $e) {
            die($e->getMessage());
        }
    }
}