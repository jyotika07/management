<?php
/*
* use namespace 
* use pdo
*/
namespace management\database;
use management\database\Connection;
use PDO;
//use class
class QueryBuilder extends Connection
{
    protected $pdo;
    public function __construct()
    {
       parent::__construct();
    }
    //select data from database
    public function select($table)
    {
        $statement = $this->pdo->prepare("select * from {$table}");
        $statement->execute();
        return $statement->fetchAll();
    }
    public function insert($table,$parameters) {
        $sql=sprintf(
            'insert into %s (%s) values (%s)',
             $table,
             implode(',',array_keys($parameters)),
             ':'.implode(', :',array_keys($parameters))
        );
        $statement=$this->pdo->prepare($sql);
        $statement->execute($parameters);
    }
}
