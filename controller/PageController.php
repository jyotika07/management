<?php
namespace controller;
use management\database\QueryBuilder;
use management\database\Pages;
class PageController extends Pages {
	public function home()
	{
		require 'index.view.php';
		$data=$this->get("users");
		return $data;
		
	}
	public function users()
	{
		require 'include/header.php';
		require 'views/users.view.php';
		require 'controller/user.php';
	}
}
