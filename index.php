<?php 
require "vendor/autoload.php";
// require 'database/bootstrap.php';
use model\Router;
use model\Request;
$router = new Router;
require 'routes.php';
$router->direct(Request::uri(),Request::method());

