
<div class="wrapper  ">	
		<div class="col-10 mt-3">
			<h3>User View List</h3>
      <a class="btn-btn-primary mr-2" href="/create">Create User</a>
		<table class="table table-striped text-center">
  <thead>
    <tr>
      <th scope="col">Id</th>
      <th scope="col">Name</th>
      <th scope="col">Username</th>
      <th scope="col">Email</th>
      <th scope="col">Status</th>
      <th scope="col">Action</th>

    </tr>
  </thead>
  